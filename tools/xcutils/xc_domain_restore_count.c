#include <err.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include <xenctrl.h>
#include <xenguest.h>
#include <xc_private.h>

/**
 * Call the domain_restore_count hypercall and print the result
 */
int
main(int argc, char **argv)
{
  xc_interface *xch;
  int cnt, cmd;
  
  // Verify the correct number of arguments.
  if (argc != 2) 
    errx(1, 
	 "Usage: %s command\n" \
	 "Gets or sets the the domain restore counter in the Xen hypervisor\n" \
	 "command = 0: Get the current value of the domain restore counter\n" \
	 "command = 1: Increment the domain restore counter"\
	   " (and retrieve the new value\n",
	 argv[0]);
  
  // Open the Xen control interface.
  xch = xc_interface_open(0,0,0);
  if (!xch) 
    errx(1, "xcutils: xc_domain_restorce_count.c: failed to open control interface");
  
  cmd = atoi(argv[1]);
  printf("command: %d\n", cmd);
  
  cnt = do_domain_restore_count_hypercall(xch, cmd);
  printf("Domain restore counter: %d\n", cnt);
  
  if ( cnt < 0 )
    {
      errx(1, "Return value: %d\n", cnt);
      fflush(stdout);
    }
  
  xc_interface_close(xch);
  return cnt;
}
